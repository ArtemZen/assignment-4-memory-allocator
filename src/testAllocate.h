#ifndef LAB4_TEST_H
#define LAB4_TEST_H
void normalAllocate(void);
void oneBlockFree(void);
void twoBlocksFree(void);
void newRegionAdd(void);
void newTest(void);

#endif
