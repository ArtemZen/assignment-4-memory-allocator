#include "testAllocate.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define SIZE_OF_HEAP 5000
#define VALUE_DEFAULT 3000
#define SIZE_OF_BLOCK 300
#define TEST_HEAP_SIZE 6000

void normalAllocate() {
    printf("Test#1:Normal allocating memory test was started\n");
    void* mainHeap = heap_init(SIZE_OF_HEAP);
    debug_heap(stdout, mainHeap);
    void* textForExamle = _malloc(VALUE_DEFAULT);
    debug_heap(stdout, mainHeap);
    if (mainHeap == NULL || textForExamle == NULL || sizeof(textForExamle)!=VALUE_DEFAULT) {
        printf("    Error: allocating wasn't successfull\n");
        if(mainHeap == NULL){
           printf("         Description: error about unsuccessfull making heap\n"); 
        }
        else{
            printf("        Description: error about unsuccessfull making example text\n"); 
        }
        return;
    }
    printf("Test#1:Normal allocating memory was successfull\n");
    _free(textForExamle);
    munmap(mainHeap, SIZE_OF_HEAP);
}

void oneBlockFree() {
    printf("Test#2:Free one block test started\n");
    void* mainHeap = heap_init(SIZE_OF_HEAP);
    debug_heap(stdout, mainHeap);
    void* textForExamle1 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle1 ==NULL || sizeof(textForExamle1)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle2 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle2 ==NULL || sizeof(textForExamle2)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle3 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle3 ==NULL || sizeof(textForExamle3)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle4 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle4 ==NULL || sizeof(textForExamle4)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle5 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle5 ==NULL || sizeof(textForExamle5)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    _free(textForExamle2);
    debug_heap(stdout, mainHeap);
    _free(textForExamle1);
    _free(textForExamle3);
    _free(textForExamle4);
    _free(textForExamle5);
    printf("Test#2:Free one blocks test was finished\n");
    munmap(mainHeap, SIZE_OF_HEAP);
}


void twoBlocksFree() {
    printf("Test#3:Free two blocks test started\n");
    void* mainHeap = heap_init(SIZE_OF_HEAP);
    debug_heap(stdout, mainHeap);
    void* textForExamle1 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle1 ==NULL || sizeof(textForExamle1)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle2 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle2 ==NULL || sizeof(textForExamle2)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle3 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle3 ==NULL || sizeof(textForExamle3)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle4 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle4 ==NULL || sizeof(textForExamle4)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    void* textForExamle5 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle5 ==NULL || sizeof(textForExamle5)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    _free(textForExamle2);
    printf("\n");
    debug_heap(stdout, mainHeap);
    _free(textForExamle3);
    printf("\n");
    debug_heap(stdout, mainHeap);
    _free(textForExamle1);
    _free(textForExamle4);
    _free(textForExamle5);
    printf("Test#3:Free two blocks test was finished\n");
    munmap(mainHeap, SIZE_OF_HEAP);
}

void newRegionAdd() {
    printf("Test#4:New region adding test started\n");
    void* mainHeap = heap_init(SIZE_OF_HEAP);
    debug_heap(stdout, mainHeap);
    void* textForExamle1 = _malloc(SIZE_OF_BLOCK);
    if(textForExamle1 ==NULL || sizeof(textForExamle1)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    printf("\n");

    void* textForExamle2 = _malloc(TEST_HEAP_SIZE);
    if(textForExamle2 ==NULL || sizeof(textForExamle2)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    printf("\n");

    if (textForExamle2 == NULL) {
        printf("Error: allocating wasn't successfull        Description: error about unsuccessfull making example text\n\n");
        return;
    }

    _free(textForExamle1);
    _free(textForExamle2);
    printf("Test#4:New region adding test was finished\n");
    munmap(mainHeap, SIZE_OF_HEAP);
}

void newTest() {
    printf("Test#5:Changing address of namespace test started\n");
    void* mainHeap = heap_init(SIZE_OF_HEAP);
    debug_heap(stdout, mainHeap);
    printf("\n");
    void* textForExamle1 = _malloc(SIZE_OF_HEAP);
    if(textForExamle1 ==NULL || sizeof(textForExamle1)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    printf("\n");
    struct block_header* newForTestHeader = mainHeap;
    void* heapArterMain = mmap(newForTestHeader->contents + newForTestHeader->capacity.bytes, 4800, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    int64_t x;
    if (&x == heapArterMain) {}
    void* textForExamle2 = _malloc(500);
    if(textForExamle2 ==NULL || sizeof(textForExamle2)!=SIZE_OF_BLOCK){
        printf("    Error: unsuccessfull making\n");
    }
    debug_heap(stdout, mainHeap);
    printf("\n");
    if (textForExamle2 == NULL) {
        printf("Error: allocating wasn't successfull\n        Description: error about unsuccessfull making example text\n");
        return;
    }
    _free(textForExamle1);
    _free(textForExamle2);
    printf("Test#5:Changing address of namespace test was finished\n");
    munmap(mainHeap, SIZE_OF_HEAP);
}
