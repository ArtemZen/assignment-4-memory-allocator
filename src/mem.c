#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t sz_Region = region_actual_size( size_from_capacity((block_capacity) {query }).bytes);
    void* addr_of_pages = map_pages(addr, sz_Region, MAP_FIXED_NOREPLACE);
    struct region reg_Res = {.addr = NULL};
    if (addr_of_pages == MAP_FAILED) {
        addr_of_pages = map_pages( addr, sz_Region, 0 );
        if (addr_of_pages == MAP_FAILED) return REGION_INVALID;
    }
    
    if (addr_of_pages != MAP_FAILED) {
        block_init( addr_of_pages, (block_size) {sz_Region}, NULL );

        reg_Res.addr = addr_of_pages;

        reg_Res.size = sz_Region;

        reg_Res.extends = addr == addr_of_pages;

    }
    return reg_Res;

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region re = alloc_region( HEAP_START, initial );

  return region_is_invalid(&re) ? NULL: re.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block_splittable(block, query)) {

        void *adr_next = block -> contents + query;

        block_init(adr_next, (block_size) {block -> capacity.bytes - query}, block -> next);

        block -> capacity.bytes = query;

        block -> next = adr_next;

        return true;
    }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block -> next && mergeable(block, block -> next)) {
        block -> capacity.bytes += size_from_capacity(block -> next -> capacity).bytes;
        block-> next = block -> next -> next;
        return true;
    }
    return false;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  struct block_search_result re_bl = {.type = BSR_CORRUPTED, .block = NULL};
    if (block == NULL) return re_bl;

    while (true) {
        if(block->next == block){
          return re_bl;
        }
        if (block -> is_free){

            while (try_merge_with_next(block));
            if (block_is_big_enough(sz, block)) {
                re_bl.type = BSR_FOUND_GOOD_BLOCK;
                re_bl.block = block;
                return re_bl;
            }

        }

        if (block -> next == NULL) break;
        block = block -> next;
    }

    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result re_sear = find_good_or_last(block, query);

    if (re_sear.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(re_sear.block, query);
        re_sear.block -> is_free = false;
    }
    return re_sear;

}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(last==NULL){
    return 0;
  }
  struct region reg = alloc_region(block_after(last), query);
    if (region_is_invalid(&reg)) return NULL;
    last -> next = reg.addr;
    if (try_merge_with_next(last)) return last;
    return last -> next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    const size_t capacity = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result re_sear = try_memalloc_existing(capacity, heap_start);
    switch (re_sear.type){
      case BSR_CORRUPTED:
        return NULL;
      break;
      case BSR_REACHED_END_NOT_FOUND:
        re_sear.block = grow_heap(re_sear.block, capacity);
        re_sear = try_memalloc_existing (capacity, re_sear.block);
      break;
      default:
      break;
    }
    return re_sear.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr_n = memalloc( query, (struct block_header*) HEAP_START );
  return addr_n ? addr_n->contents: NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return;

  struct block_header* main_of_header = block_get_header(mem);
  main_of_header->is_free = true;
  while (try_merge_with_next(main_of_header));

}
