#include <stdint.h>
#include <stdlib.h>

#include "testAllocate.h"
int main() {
    normalAllocate();
    oneBlockFree();
    twoBlocksFree();
    newRegionAdd();
    newTest();
    return 0;
}
